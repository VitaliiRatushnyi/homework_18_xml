package com.epam.comparator;

import com.epam.model.Knife;

import java.util.Comparator;

public class BladeComparator implements Comparator<Knife> {
  public int compare(Knife o1, Knife o2) {
    return Integer.compare(o1.getVisual().getBladeLength(),
        o2.getVisual().getBladeLength()) * -1;
  }
}
