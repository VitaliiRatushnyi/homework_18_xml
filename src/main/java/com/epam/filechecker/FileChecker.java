package com.epam.filechecker;

import org.apache.commons.io.FilenameUtils;

import java.io.File;

public class FileChecker {
  public static boolean isXML(File xml) {
    return xml.isFile() && FilenameUtils.getExtension(xml.getName()).equals("xml");
  }

  public static boolean isXML(String path) {
    File file = new File(path);
    return file.isFile() && FilenameUtils.getExtension(file.getName()).equals("xml");
  }

  public static boolean isXSD(File xsd){
    return xsd.isFile() && FilenameUtils.getExtension(xsd.getName()).equals("xsd");
  }

  public static boolean isXSD(String path){
    File file = new File(path);
    return file.isFile() && FilenameUtils.getExtension(file.getName()).equals("xsd");
  }
}