package com.epam;

import com.epam.model.Knife;
import com.epam.comparator.BladeComparator;
import com.epam.filechecker.FileChecker;
import com.epam.parsers.DomParser;
import com.epam.parsers.SaxParser;
import com.epam.parsers.StaxParser;

import java.util.List;

public class Main {

  public static void main(String[] args) {

    String xmlFilePath = "src\\main\\resources\\knife.xml";

    if (FileChecker.isXML(xmlFilePath)) {
      printList(DomParser.parseFile(xmlFilePath), "DOM Parser");
      printList(SaxParser.parseFile(xmlFilePath), "SAX Parser");
      printList(StaxParser.parseFile(xmlFilePath), "StAX Parser");
    }
  }

  private static void printList(List<Knife> knives, String parserName) {
    System.out.println("Parser name: " + parserName);
    System.out.println("--------------------------------");
    knives.sort(new BladeComparator());
    for (Knife knife : knives) {
      System.out.println(knife);
    }
    System.out.println();
  }
}
