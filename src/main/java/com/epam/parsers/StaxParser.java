package com.epam.parsers;

import com.epam.model.Knife;
import com.epam.model.Visual;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class StaxParser {

  public static List<Knife> parseFile(String filePath) {
    List<Knife> knives = new ArrayList<>();
    Knife knife = null;
    Visual visual = null;

    try {
      XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(new File(filePath)));

      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case "knife":
              knife = new Knife();

              Attribute idAttr = startElement.getAttributeByName(new QName("id"));
              if (idAttr != null) {
                knife.setId(Integer.parseInt(idAttr.getValue()));
              }
              break;
            case "type":
              xmlEvent = xmlEventReader.nextEvent();
              assert knife != null;
              knife.setType(xmlEvent.asCharacters().getData());
              break;
            case "handy":
              xmlEvent = xmlEventReader.nextEvent();
              assert knife != null;
              knife.setHandy(xmlEvent.asCharacters().getData());
              break;
            case "origin":
              xmlEvent = xmlEventReader.nextEvent();
              assert knife != null;
              knife.setOrigin(xmlEvent.asCharacters().getData());
              break;
            case "visual":
              xmlEvent = xmlEventReader.nextEvent();
              visual = new Visual();
              break;
            case "bladeLength":
              xmlEvent = xmlEventReader.nextEvent();
              assert visual != null;
              visual.setBladeLength(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "bladeWidth":
              xmlEvent = xmlEventReader.nextEvent();
              assert visual != null;
              visual.setBladeWidth(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            case "bladeMaterial":
              xmlEvent = xmlEventReader.nextEvent();
              assert visual != null;
              visual.setBladeMaterial(xmlEvent.asCharacters().getData());
              break;
            case "handleMaterial":
              xmlEvent = xmlEventReader.nextEvent();
              assert visual != null;
              visual.setHandleMaterial(xmlEvent.asCharacters().getData());
              break;
            case "value":
              xmlEvent = xmlEventReader.nextEvent();
              assert knife != null;
              knife.setValue(Boolean.parseBoolean(xmlEvent.asCharacters().getData()));
              assert visual != null;
              knife.setVisual(visual);
              break;
          }
        }

        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          if (endElement.getName().getLocalPart().equals("knife")) {
            knives.add(knife);
          }
        }
      }

    } catch (FileNotFoundException | XMLStreamException e) {
      e.printStackTrace();
    }
    return knives;
  }
}
