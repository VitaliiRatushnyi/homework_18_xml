package com.epam.parsers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import com.epam.model.Knife;
import com.epam.model.Visual;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

public class DomParser {

  private DomParser() {
  }

  public static List<Knife> parseFile(String path) {

    Document doc = createDoc(path);
    assert doc != null;
    doc.getDocumentElement().normalize();

    List<Knife> knives = new ArrayList<>();
    NodeList nodeList = doc.getElementsByTagName("knife");

    for (int i = 0; i < nodeList.getLength(); i++) {
      Knife knife = new Knife();
      Visual visual;

      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;

        knife.setId(Integer.parseInt(element
            .getAttribute("id")));
        knife.setType(element
            .getElementsByTagName("type").item(0).getTextContent());
        knife.setHandy(element
            .getElementsByTagName("handy").item(0).getTextContent());
        knife.setOrigin(element
            .getElementsByTagName("origin").item(0).getTextContent());
        knife.setValue(Boolean.parseBoolean(element
            .getElementsByTagName("value").item(0).getTextContent()));

        visual = getVisual(element.getElementsByTagName("visual"));

        knife.setVisual(visual);
        knives.add(knife);
      }
    }
    return knives;
  }

  private static Document createDoc(String path) {
    try {
      File inputFile = new File(path);
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      return dBuilder.parse(inputFile);
    } catch (ParserConfigurationException | IOException | SAXException e) {
      e.printStackTrace();
    }
    return null;
  }

  private static Visual getVisual(NodeList visualList) {
    Visual visual = new Visual();

    Node node = visualList.item(0);
    if (node.getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) node;

      visual.setBladeLength(Integer.parseInt(element
          .getElementsByTagName("bladeLength").item(0).getTextContent()));
      visual.setBladeWidth(Integer.parseInt(element
          .getElementsByTagName("bladeWidth").item(0).getTextContent()));
      visual.setBladeMaterial(element
          .getElementsByTagName("bladeMaterial").item(0).getTextContent());
      visual.setHandleMaterial(element
          .getElementsByTagName("handleMaterial").item(0).getTextContent());
    }
    return visual;
  }
}
