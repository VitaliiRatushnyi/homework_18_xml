package com.epam.parsers;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.epam.model.Knife;
import com.epam.model.Visual;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class SaxParser {

  public static List<Knife> parseFile(String path) {
    try {
      File inputFile = new File(path);
      SAXParserFactory factory = SAXParserFactory.newInstance();
      SAXParser saxParser = factory.newSAXParser();
      SaxHandler saxhandler = new SaxHandler();
      saxParser.parse(inputFile, saxhandler);
      return saxhandler.getKnives();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return new ArrayList<>();
  }

  private static class SaxHandler extends DefaultHandler {
    List<Knife> knives = new ArrayList<>();
    Knife knife = null;
    Visual visual = null;

    boolean type = false;
    boolean handy = false;
    boolean origin = false;
    boolean bladeLength = false;
    boolean bladeWidth = false;
    boolean bladeMaterial = false;
    boolean handleMaterial = false;
    boolean value = false;

    List<Knife> getKnives() {
      return this.knives;
    }

    @Override
    public void startElement(
        String uri, String localName, String qName, Attributes attributes) {

      if (qName.equalsIgnoreCase("knife")) {
        String id = attributes.getValue("id");
        knife = new Knife();
        knife.setId(Integer.parseInt(id));
      } else if (qName.equalsIgnoreCase("type")) {
        type = true;
      } else if (qName.equalsIgnoreCase("handy")) {
        handy = true;
      } else if (qName.equalsIgnoreCase("origin")) {
        origin = true;
      } else if (qName.equalsIgnoreCase("bladeLength")) {
        bladeLength = true;
      } else if (qName.equalsIgnoreCase("bladeWidth")) {
        bladeWidth = true;
      } else if (qName.equalsIgnoreCase("bladeMaterial")) {
        bladeMaterial = true;
      } else if (qName.equalsIgnoreCase("handleMaterial")) {
        handleMaterial = true;
      } else if (qName.equalsIgnoreCase("value")) {
        value = true;
      }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
      if (qName.equalsIgnoreCase("knife")) {
        knives.add(knife);
      }
    }

    @Override
    public void characters(char ch[], int start, int length) {
      if (type) {
        knife.setType(new String(ch, start, length));
        type = false;
      } else if (handy) {
        knife.setHandy(new String(ch, start, length));
        handy = false;
      } else if (origin) {
        knife.setOrigin(new String(ch, start, length));
        origin = false;
      } else if (bladeLength) {
        visual = new Visual();
        visual.setBladeLength(Integer.parseInt(new String(ch, start, length)));
        bladeLength = false;
      } else if (bladeWidth) {
        visual.setBladeWidth(Integer.parseInt(new String(ch, start, length)));
        bladeWidth = false;
      } else if (bladeMaterial) {
        visual.setBladeMaterial(new String(ch, start, length));
        bladeMaterial = false;
      } else if (handleMaterial) {
        visual.setHandleMaterial(new String(ch, start, length));
        knife.setVisual(visual);
        handleMaterial = false;
      } else if (value) {
        knife.setValue(Boolean.parseBoolean(new String(ch, start, length)));
        value = false;
      }
    }
  }
}
