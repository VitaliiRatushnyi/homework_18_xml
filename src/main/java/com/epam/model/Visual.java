package com.epam.model;

public class Visual {

  private int bladeLength;
  private int bladeWidth;
  private String bladeMaterial;
  private String handleMaterial;

  public Visual() {
  }

  public Visual(int bladeLength, int bladeWidth, String bladeMaterial, String handleMaterial) {
    this.bladeLength = bladeLength;
    this.bladeWidth = bladeWidth;
    this.bladeMaterial = bladeMaterial;
    this.handleMaterial = handleMaterial;
  }

  public int getBladeLength() {
    return bladeLength;
  }

  public void setBladeLength(int bladeLength) {
    this.bladeLength = bladeLength;
  }

  public int getBladeWidth() {
    return bladeWidth;
  }

  public void setBladeWidth(int bladeWidth) {
    this.bladeWidth = bladeWidth;
  }

  public String getBladeMaterial() {
    return bladeMaterial;
  }

  public void setBladeMaterial(String bladeMaterial) {
    this.bladeMaterial = bladeMaterial;
  }

  public String getHandleMaterial() {
    return handleMaterial;
  }

  public void setHandleMaterial(String handleMaterial) {
    this.handleMaterial = handleMaterial;
  }

  @Override
  public String toString() {
    return "Visual{" +
        "bladeLength=" + bladeLength +
        ", bladeWidth=" + bladeWidth +
        ", bladeMaterial='" + bladeMaterial + '\'' +
        ", handleMaterial='" + handleMaterial + '\'' +
        '}';
  }
}
