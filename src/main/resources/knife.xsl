<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="/">
        <html>
            <head>
                <style type="text/css">
                    table {
                    border: 1px ;
                    width: 100%;
                    }

                    th {
                    font-size: 20px;
                    border: 1px solid gray;
                    background-color: #8c8c8c;
                    color: black;
                    text-align: center;
                    border-radius: 2px;
                    }

                    td {
                    border: 1px solid gray;
                    background-color: #b3b3b3;
                    color: white;
                    text-shadow: 2px 2px black
                    text-align: left;
                    border-radius: 5px;
                    padding: 4px;
                    margin: 3px;
                    }
                </style>
            </head>

            <body>
                <table>
                    <tr>
                        <th>ID</th>
                        <th>Type</th>
                        <th>Handy</th>
                        <th>Origin</th>
                        <th>Blade Length</th>
                        <th>Blade Width</th>
                        <th>Blade Material</th>
                        <th>Handy Material</th>
                        <th>Value</th>
                    </tr>

                    <xsl:for-each select="knives/knife">
                        <tr>
                            <td><xsl:value-of select="@id"/></td>
                            <td><xsl:value-of select="type"/></td>
                            <td><xsl:value-of select="handy"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="visual/bladeLength"/></td>
                            <td><xsl:value-of select="visual/bladeWidth"/></td>
                            <td><xsl:value-of select="visual/bladeMaterial"/></td>
                            <td><xsl:value-of select="visual/handleMaterial"/></td>
                            <td><xsl:value-of select="value"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
